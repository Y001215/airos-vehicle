/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once
#include <string>

namespace air {
namespace net {

class LtevDsmp {
 public:
  LtevDsmp();
  ~LtevDsmp();

  bool Decode(const std::string& str_in, uint16_t* aid,
              std::string* str_out) const;
  bool Encode(const std::string& str_in, uint16_t aid,
              std::string* str_out) const;

 private:
  bool use_adapta_layer_{true};
  static constexpr uint8_t dsmp_version_{0x00};
  static constexpr uint8_t protocol_type_{0x04};
};

}  // namespace net
}  // namespace air
