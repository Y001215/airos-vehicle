CMAKE_MINIMUM_REQUIRED(VERSION 3.16)

### source
FILE(GLOB SRCS "*.cc")

### lib
ADD_LIBRARY(air_pc5 OBJECT ${SRCS})

### test
ADD_EXECUTABLE(pc5_test
    test/main.cpp
    $<TARGET_OBJECTS:air_pc5>
)
