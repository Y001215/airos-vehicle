/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include <iostream>
#include "pc5/module_udp.h"
#include "pc5/ltevdsmp.h"

int main(int argc, char* argv[]) {
  std::shared_ptr<::air::net::ModuleUdp::ModuleInterface> ptr;
  ptr.reset(&::air::net::ModuleUdp::getInstance());
  ptr->Init("172.26.20.135", 10005, 10005);
  ::air::net::LtevDsmp ds;
  while (true) {
    std::string stra("helloworld");
    std::string strb;
    ds.Encode(stra, 100, &strb);
    ptr->Send(strb, 100);
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    std::string str;
    ptr->Recv(&str);
    std::string strc;
    uint16_t aid = -1;
    ds.Decode(str, &aid, &strc);
    std::cout << "******************:" << strc << std::endl;
  }
  return 0;
}
