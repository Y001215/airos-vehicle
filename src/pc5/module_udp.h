/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include <deque>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <memory>
#include <functional>
#include <atomic>
#include <string>

#include "pc5/module_interface.h"
#include "pc5/singleton.h"

namespace air {
namespace net {

class ModuleUdp : public ModuleInterface, public Singleton<ModuleUdp> {
  friend class Singleton<ModuleUdp>;

 private:
  int sock_{-1};
  struct sockaddr_in addr_in_;
  struct sockaddr_in addr_out_;

  int cbr_{0};

 public:
  ModuleUdp();
  ~ModuleUdp();

  bool Init(const std::string& peer_ip, int peer_port, int local_port) override;
  bool Send(const std::string& data, int aid) override;
  bool Recv(std::string* str_out) override;
  int GetCbr() override;

 private:
  bool HeaderEncode(const std::string& str_in, std::string* str_out, int aid);
  bool HeaderDecode(const std::string& str_in, std::string* str_out);
};

}  // namespace net
}  // namespace air
