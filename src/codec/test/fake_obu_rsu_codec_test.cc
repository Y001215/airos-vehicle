/******************************************************************************
* Copyright 2022 The Airos Authors. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/
#include <glog/logging.h>
#include <gtest/gtest.h>

#include "codec/fake_obu_rsu_codec/fake_obu_rsu_codec.h"

TEST(fake_obu_rsu_codec, test) {
  std::string data = "hello,world";
  auto pack = air::codec::FakeObuRsuCodec::Encode(
    uint8_t(air::codec::FakeObuRsuCodec::MsgType::kSpat), data);

  std::string de_data;
  uint8_t t;
  EXPECT_EQ(0, air::codec::FakeObuRsuCodec::Decode(pack, &t, &de_data));
  EXPECT_TRUE(t == uint8_t(air::codec::FakeObuRsuCodec::MsgType::kSpat));
  LOG(INFO) << "decode data: " << de_data;
}
