/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "framework/actor.h"
#include "framework/msg.h"

namespace air {
namespace api {
using CallBackHander = std::function<void(const std::string&)>;
class TrafficProtoApi : public air::link::Actor {
 public:
  int Init(const char* param) override { return 0; }
  void Proc(const std::shared_ptr<const air::link::Msg>& msg) override;
  void RegisterCallBack(
    const std::function<void(const std::string&)>& traffic_pb_func) {
    traffic_pb_func_ = traffic_pb_func;
  }
 private:
  CallBackHander traffic_pb_func_ { nullptr };
};

class AsnApi : public air::link::Actor {
 public:
  int Init(const char* param) override { return 0; }
  void Proc(const std::shared_ptr<const air::link::Msg>& msg) override;
  void RegisterCallBack(
    const std::function<void(const std::string&)>& asn_func) {
    asn_func_ = asn_func;
  }
 private:
  CallBackHander asn_func_ { nullptr };
};

class VehicleBasicInfoApi : public air::link::Actor {
 public:
  int Init(const char* param) override;
  void Proc(const std::shared_ptr<const air::link::Msg>& msg) override;
 private:
  std::vector<std::string> send_list_;
};

}  // namespace api
}  // namespace air
