/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#include <memory>

#include <glog/logging.h>

#include "components/air_api_service/func_handle.h"
#include "v2xpb-asn-message-frame.pb.h"
#include "v2x-asn-msgs-adapter.hpp"
#include "config/config.h"

namespace air {
namespace api {

void TrafficProtoApi::Proc(const std::shared_ptr<const air::link::Msg>& msg) {
  traffic_pb_func_(msg->GetData());
  return;
}

void AsnApi::Proc(const std::shared_ptr<const air::link::Msg>& msg) {
  asn_func_(msg->GetData());
  return;
}

int VehicleBasicInfoApi::Init(const char* param) {
  send_list_ = air::config::Config::GetInstance()
    ->GetSendList(GetActorName());
  return 0;
}

void VehicleBasicInfoApi::Proc(
  const std::shared_ptr<const air::link::Msg>& msg) {
  auto str_msg = std::make_shared<air::link::Msg>(*msg);
  for (int i = 0; i < send_list_.size(); ++i) {
    Send(send_list_[i], str_msg);
  }
  Send(msg->GetSrc(), std::make_shared<air::link::Msg>());
  return;
}

}  // namespace api
}  // namespace air
