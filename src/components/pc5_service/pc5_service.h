/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#include <memory>
#include <string>
#include <vector>

#include "framework/worker.h"
#include "pc5/ltevdsmp.h"
#include "pc5/module_interface.h"
#include "pc5/module_udp.h"
#include "v2x-asn-msgs-adapter.hpp"

namespace air {
namespace net {
class ModuleInterface;
class Pc5ServiceReceiver : public air::link::Worker {
 public:
  void OnInit() override;

  void Run() override;

  void OnExit() override;

 private:
  std::shared_ptr<ModuleInterface> sptr_module_;
  LtevDsmp dsmp_;
  EnAsnType asn_type_;
  std::vector<std::string> send_list_;
};

class Pc5ServiceSender : public air::link::Worker {
 public:
  void OnInit() override;

  void Run() override;

  void OnExit() override;
 private:
  std::shared_ptr<ModuleInterface> sptr_module_;
  LtevDsmp dsmp_;
  EnAsnType asn_type_;
};

}  // namespace net
}  // namespace air
