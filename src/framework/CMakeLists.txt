CMAKE_MINIMUM_REQUIRED(VERSION 3.16)

### configure file
CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/flags.cc.in ${CMAKE_CURRENT_SOURCE_DIR}/flags.cc @ONLY)

### source
FILE(GLOB FRAMEWORK_SRCS "*.cc" "*.c")

### lib
ADD_LIBRARY(air_framework OBJECT ${FRAMEWORK_SRCS})

### install
FILE(GLOB header_files 
    log.h
    msg.h
    actor.h
    event.h
    worker.h
    mod_lib.h
    mod_manager.h
    app.h
    common.h
    flags.h
)
INSTALL(FILES 
    ${header_files}
    PERMISSIONS
    OWNER_READ
    GROUP_READ
    WORLD_READ
    DESTINATION ${FRAMEWORK_INC_DIR}/framework
)
