/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#pragma once
#include <functional>
#include <string>
#include <mutex>

using recv_handle =
    std::function<void(const std::string& src, const std::string& data)>;

namespace air {
namespace net {

enum class NetOption : int {
  // 服务器地址,比如ws://127.0.0.1:8765
  kServerUrl = 0,
  // 客户端id，比如mqtt的client id
  kClientId = 1,
  // 用户账号
  kClientUsername = 2,
  // 用户密码
  kClientPassword = 3,
  // 根证书路径
  kSslFilepathCa = 4,
  // 用户数字证书路径
  kSslFilepathClientCert = 5,
  // 用户私钥路径
  kSslFilePathClientKey = 6,
  // 重连时间间隔
  kReconnectTimeSec = 7,
  // 设置本地地址
  kLocalUrl = 8,
};

/**
 * @brief 网络协议统一接口
 */
class NetInterface {
 public:
  NetInterface() = default;
  virtual ~NetInterface() {}

  /**
   * SetOption() - 设置网络参数
   * @opt:  见枚举 NetOption
   * @val:  值
   * 
   * @return: -1 设置失败, 0 设置成功
   */
  virtual int SetOption(NetOption opt, const std::string& val) = 0;

  /**
   * Connect() - 连接服务器，并启动事件循环
   * 
   * @return: -1 设置失败, 0 设置成功
   */
  virtual int Connect() = 0;

  /**
   * Send() - 发送数据
   * @dst:    mqtt可以填写要发送的topic，其它协议可以不填
   * @data:   数据
   * 
   * @return: -1 设置失败, 0 设置成功
   */
  virtual int Send(const std::string& dst, const std::string& data) = 0;

  /**
   * SetRecvHandle() - 设置接收消息回调，设置后就不能通过Recv接收消息
   * @recv_handle: 回调函数
   * 
   * @return: -1 设置失败，0 设置成功
   */
  virtual int SetRecvHandle(recv_handle) {
    return -1;
  }

  /**
   * Recv() - 阻塞接收服务器数据
   * @src: mqtt返回topic，其它协议为空
   * @data: 数据
   * @return: -1 设置失败，0 设置成功
   */
  virtual int Recv(std::string* src, std::string* data) {
    return -1;
  }
};

}  // namespace net
}  // namespace air
