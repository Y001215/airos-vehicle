CMAKE_MINIMUM_REQUIRED(VERSION 3.16)
PROJECT(@template_name@)

#### compile setting
SET(CMAKE_C_STANDARD 11)
SET(CMAKE_CXX_STANDARD 11)
SET(CMAKE_C_FLGAS "-fPIC -DPIC")
SET(CMAKE_CXX_FLGAS "-fPIC -DPIC")

#### path setting
SET(CMAKE_INSTALL_PREFIX "$ENV{HOME}/airos-vehicle")
SET(@template_name@_INSTALL_SERVICE_DIR ${CMAKE_INSTALL_PREFIX}/service)
SET(@template_name@_INSTALL_LIB_DIR ${CMAKE_INSTALL_PREFIX}/lib)

#### dep 3rd lib
FIND_PACKAGE(jsoncpp REQUIRED)
FIND_PACKAGE(gflags REQUIRED)
FIND_PACKAGE(glog REQUIRED)

#### include directory
INCLUDE_DIRECTORIES(${CMAKE_INSTALL_PREFIX}/include)

#### lib path
LINK_LIBRARIES(
  glog::glog
  gflags
  dl m
  -Wl,-z,defs
)
LINK_DIRECTORIES(${CMAKE_INSTALL_PREFIX}/lib)

#### genrate mod lib
ADD_LIBRARY(${PROJECT_NAME} SHARED
    src/${PROJECT_NAME}.cc
)
TARGET_LINK_LIBRARIES(${PROJECT_NAME}
    air-link
)

#### install
INSTALL(TARGETS ${PROJECT_NAME}
    LIBRARY DESTINATION ${@template_name@_INSTALL_LIB_DIR}
    ARCHIVE DESTINATION ${@template_name@_INSTALL_LIB_DIR}
    RUNTIME DESTINATION ${@template_name@_INSTALL_LIB_DIR}
)
FILE(GLOB service_files "service/*.json")
INSTALL(FILES 
    ${service_files}
    PERMISSIONS
    OWNER_READ
    GROUP_READ
    WORLD_READ
    DESTINATION ${@template_name@_INSTALL_SERVICE_DIR}
)